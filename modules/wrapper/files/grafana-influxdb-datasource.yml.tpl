# https://github.com/helm/charts/tree/master/stable/grafana#sidecar-for-datasources
# config file version
apiVersion: 1
# list of datasources that should be deleted from the database
deleteDatasources:
    - name: influxdb
      orgId: 1
# list of datasources to insert/update depending
# whats available in the database
datasources:
    - name: influxdb
      # <string, required> datasource type. Required
      type: influxdb
      # <string, required> access mode. proxy or direct (Server or Browser in the UI). Required
      access: proxy
      # <int> org id. will default to orgId 1 if not specified
      orgId: 1
      url: http://influxdb.home-assistant.svc:8086
      password: ''
      user: admin
      database: home_assistant
      basicAuth: false
      basicAuthUser: ''
      basicAuthPassword: ''
      withCredentials: false
      isDefault: false
      jsonData:
        httpMode: POST
plugins:
    []
    # - digrich-bubblechart-panel
    # - grafana-clock-panel
