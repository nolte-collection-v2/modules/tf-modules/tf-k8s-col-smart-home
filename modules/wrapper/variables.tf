variable "namespace" {
  default = ""
}

variable "node_red_extra_values" {
  default = {}
}

variable "broker_extra_values" {
  default = {}
}

variable "home_assistant_extra_values" {
  default = {}
}

variable "influxdb_extra_values" {
  default = {}
}

variable "esphome_extra_values" {
  default = {}
}

variable "grafana_influx_datasource_enabled" {
    default = false
}
