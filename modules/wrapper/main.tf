
module "broker" {
  source = "git::https://gitlab.com/nolte-collection-v2/modules/tf-modules/tf-k8s-mosquitto.git//modules/install"
  extra_values = var.broker_extra_values
  namespace    = var.namespace
}

module "node_red" {
  source = "git::https://gitlab.com/nolte-collection-v2/modules/tf-modules/tf-k8s-node-red.git//modules/install"
  extra_values = var.node_red_extra_values
  namespace    = var.namespace
}



module "influxdb" {
  source     = "git::https://gitlab.com/nolte-collection-v2/modules/tf-modules/tf-k8s-influxdb.git//modules/install"
  extra_values = var.influxdb_extra_values
  namespace    = var.namespace
}


resource "kubernetes_secret" "datasource_influx" {
  count = var.grafana_influx_datasource_enabled ? 1 : 0
  depends_on = [module.influxdb]
  metadata {
    name      = "grafana-influx-datasource"
    namespace = "monitoring-presentation"
    labels = {
      grafana_datasource = "1"
    }
  }
  data = {
    "datasource-influx.yaml" = "${templatefile("${path.module}/files/grafana-influxdb-datasource.yml.tpl", {

    })}"
  }

}

module "home_assistant" {
  depends_on = [module.broker,module.influxdb]
  source     = "git::https://gitlab.com/nolte-collection-v2/modules/tf-modules/tf-k8s-home-assistant.git//modules/install"
  extra_values = var.home_assistant_extra_values
  namespace    = var.namespace
}

module "esphome" {
  source     = "git::https://gitlab.com/nolte-collection-v2/modules/tf-modules/tf-k8s-esphome.git//modules/install"
  extra_values = var.esphome_extra_values
  namespace    = var.namespace
}
