resource "kubernetes_namespace" "acc_namespace" {
  metadata {
    name = "acc-tf-k8s-col-smart-home"
  }
}

module "install" {
  source = "../../modules/wrapper"
  namespace = kubernetes_namespace.acc_namespace.metadata[0].name
}

output "helm_release" {
  value = module.install.release
}